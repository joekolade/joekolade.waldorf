

// Fetch Namespace
var Joekolade = Joekolade || {};

Joekolade.waldorfWebsite = {

	makeBgFullHeight: function(){
		$('body').imagesLoaded(
			function(){
				var h = $('body').outerHeight() > $('html').outerHeight() ? $('body').outerHeight() : $('html').outerHeight();
				$('.bg-image, .bg-sign').css({
					'min-height': h
				}).addClass('show');
		});
	},

	init: function(){
		var _self = this;

		$(window).on('resize', function () {
			_self.makeBgFullHeight();
		});
		$(window).resize();
	}
};


document.addEventListener('DOMContentLoaded', function() {

	Joekolade.waldorfWebsite.init();

	// Hamburger toggle
	var hamburger = document.querySelector('.menu-toggle');

	hamburger.addEventListener('click', function () {
		this.classList.add('active');
		return false;
	});

	// Accordion footer
	var accordionLinks = document.querySelectorAll('.sitemap .toggle-header');

	Array.from(accordionLinks).forEach(function(link){
		link.addEventListener('click', function(event) {
			var
				$nav = $(this).next('nav'),
				$ul = $('> ul', $nav),
				ul = $ul.get(0)
			;

			if(ul.classList.contains('open')){
				ul.classList.remove('open');
			}
			else {
				var uls = document.querySelectorAll('footer > section > nav > ul');
				Array.from(uls).forEach(function(ul){
					ul.classList.remove('open');
				});
				ul.classList.add('open');
			}
		});
	});
});
